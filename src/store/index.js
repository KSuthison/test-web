import { createStore } from 'redux'
import rootReducer from '../reducers/index'
import thunk from 'redux-thunk'
import { applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

const persistConfig = {
    key: 'root',
    storage: storage,
    whitelist: ['token']
}
const persistedReducer = persistReducer(persistConfig, rootReducer)

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    let store = createStore(persistedReducer,
        storeEnhancers(applyMiddleware(thunk))) //add custom middleware here applyMiddleware(customMiddleware,thunk)
    let persistor = persistStore(store)
    return { store, persistor }
}