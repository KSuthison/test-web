import React from 'react'
class CreateContentComponent extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div>
                <div><input onChange={this.props.handleNameChange} placeholder="Name"></input></div>
                <div><input onChange={this.props.handleStatusChange} placeholder="Status"></input></div>
                <div><input onChange={this.props.handleCategoryChange} placeholder="Category"></input></div>
                <div><input onChange={this.props.handleAuthorChange} placeholder="Author"></input></div>
                <div><textarea type="area" onChange={this.props.handleContentChange} placeholder="Content"></textarea></div>
                <button onClick={this.props.handleCreateContentClick}>Create new content</button>
            </div>
        )
    }
} 
export default CreateContentComponent