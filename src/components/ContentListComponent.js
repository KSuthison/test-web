import React from 'react'
import { Card, CardImg, CardTitle, CardSubtitle, CardText, Button, CardBody } from 'reactstrap'
class ContentListComponent extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const contentsArray = this.props.contentList.map(content => (
            <Card key={content.id}>
                <CardBody>
                    <CardTitle>{content.name}</CardTitle>
                    <CardSubtitle>{content.status}</CardSubtitle>
                    <CardSubtitle>{content.category}</CardSubtitle>
                    <CardSubtitle>{content.author}</CardSubtitle>
                    <CardText>{content.content}</CardText>
                    <CardSubtitle>{content.created_date}</CardSubtitle>
                    <Button style={{backgroundColor:"#B75D69"}} onClick={() => this.props.handleDeleteContentClick(content.id)}>Delete</Button>
                </CardBody>
            </Card>
        ));
        return (
            <div>
                {contentsArray}
                <Card name="sdf" />
            </div>
        )
    }
}
export default ContentListComponent