import React, { Component } from 'react'
import axios from 'axios'

class LoginComponent extends React.Component {

    constructor(props) {
        super(props)

    }

    render() {
        return (
            <div>
                <div>
                    <input placeholder="Username"
                        onChange={this.props.handleChangeUsername} />
                </div>
                <div>
                    <input placeholder="Password"
                        type="password"
                        onChange={this.props.handleChangePassword} />
                </div>
                <div style={{color:"#B75D69"}}>
                    {this.props.loginError}
                </div>
                <button style={{width:'200px'}} onClick={this.props.handleLoginClick.bind(this)}>Login</button>
                <div>
                    Don't have and account?
                        <div>
                        <button style={{width:'200px'}} onClick={this.props.handleRegisterClick.bind(this)}>Register</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginComponent