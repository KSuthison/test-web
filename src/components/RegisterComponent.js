import React from 'react'

class RegisterComponent extends React.Component {
    render() {
        return (
            <div>
                <div>
                    <div>
                        <input placeholder="Username"
                            onChange={this.props.handleChangeUsername} />
                    </div>
                    <div>
                        <input placeholder="Password"
                            type="password"
                            onChange={this.props.handleChangePassword} />
                    </div>
                    <div style={{ color: "#B75D69" }}>{this.props.registerError}</div>
                    <div>
                        <div>
                            <button onClick={this.props.handleRegisterClick.bind(this)}>Register</button>
                        </div>
                        <button onClick={this.props.handleRegisterBack.bind(this)}>Back</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default RegisterComponent