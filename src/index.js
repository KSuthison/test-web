import React from 'react'
import ReactDOM from 'react-dom'
import LandingContainer from './containers/LandingContainer'
import ContentContainers from './containers/ContentsContainer'
import PrivateRoute from './PrivateRoute'
import { Provider } from 'react-redux'
import configureStore from './store'
import { PersistGate } from 'redux-persist/integration/react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();
const { store, persistor } = configureStore()

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <Router history={history}>
                <Switch>
                    <Route exact path="/login" component={LandingContainer} />
                    <PrivateRoute exact path="/" component={ContentContainers} />
                </Switch>
            </Router>
        </PersistGate>
    </Provider>
    , document.getElementById('root'))