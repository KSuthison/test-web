import React from 'react'
import {Route,Redirect} from 'react-router-dom'
import configureStore from './store/index'
import { connect } from 'react-redux'
import api from './services/ApiService'
 
function PrivateRoute({ component: Component,token:token, ...rest }) {
  
  console.log(`private route token = ${token}`)
    return (
      <Route
        {...rest}
        render={props =>
          token!==null ? (
            <Component 
            api={api(token)}
            {...props} 
            />
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }

  const mapStateToProps = state => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(PrivateRoute)