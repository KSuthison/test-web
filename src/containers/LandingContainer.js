import React, { Component } from 'react'
import axios from 'axios'
import LoginComponent from '../components/LoginComponent'
import RegisterComponent from '../components/RegisterComponent'
import { connect } from 'react-redux'
import { getData, setToken } from "../actions/index";
import api from '../services/ApiService'
import { Container, Row, Col } from 'reactstrap';
import { Redirect } from 'react-router-dom'

class LandingContainer extends React.Component {

    constructor(props) {
        super(props)
        this.API = api()
        this.state = {
            loginUsername: "",
            loginPassword: "",
            registerUsername: "",
            registerPassword: "",
            isOnRegister: false,
            registerError: "",
            loginError: ""
        }
    }

    /*
     * LOGIN
    */
    handleLoginUsernameChange = (event) => {
        this.setState({
            loginUsername: event.target.value
        })
    }
    handleLoginPasswordChange = (event) => {
        this.setState({
            loginPassword: event.target.value
        })
    }
    handleLoginRegisterClick = (event) => {
        console.log("register")
        this.setState({
            isOnRegister: true
        })
    }
    handleLoginClick = (event) => {
        const userInfo = {
            username: this.state.loginUsername,
            password: this.state.loginPassword
        }
        this.fetchLogin(userInfo)

    }
    fetchLogin = (userInfo) => {
        this.API.post(`/user/login`, userInfo)
            .then(res => {
                console.log('login token = ' + res.data.token)
                this.props.setToken(res.data.token)
                this.props.history.push('/')
                console.log('login token from store after login = ' + this.props.token)
                console.log(JSON.parse(localStorage.getItem('persist:root')).token)
                document.title = `Welcome! ${this.state.loginUsername}`;
            })
            .catch(err => {
                console.log(err.response.data.message)
                this.setState({
                    loginError: `${err.message}: ${err.response.data.message}`
                })
            })
    }

    /*
     * Register
    */

    handleRegisterUsernameChange = (event) => {
        this.setState({
            registerUsername: event.target.value
        })
    }
    handleRegisterPasswordChange = (event) => {
        this.setState({
            registerPassword: event.target.value
        })
    }
    handleRegisterClick = () => {
        const userInfo = {
            username: this.state.registerUsername,
            password: this.state.registerPassword
        }
        this.API.post(`/user/register`, userInfo)
            .then(res => {
                console.log(res)
                this.props.setToken(res.data.token)
                this.props.history.push('/')
            }
            ).catch(err => {
                console.log(err.message)
                this.setState({
                    registerError: `${err.message}: ${err.response.data.message}`
                })
            })
    }
    handleRegisterBack = () => {
        this.setState({
            isOnRegister: false
        })
    }

    componentDidMount() {
        this.props.getData()
    }

    render() {
        return (
            <div>
                <Row style={{ display:'flex', height: '100vh', margin: '0 auto', display: 'flex', justifyContent: 'center' }}>
                    <Col xs={5} className="text-center" style={{display:'flex', color: '#ffffff', backgroundColor: '#00A7E1' }}>
                        <div style={{fontSize:'40px', width:'100vh', alignSelf:'center',textAlign:'center'}}>Welcome !</div>
                        </Col>
                    <Col xs={7} style={{ backgroundColor: '#fffff',alignSelf:'center',textAlign:'center' }}>
                        {(!this.state.isOnRegister)
                            ? <LoginComponent
                                loginError={this.state.loginError}
                                handleChangePassword={this.handleLoginPasswordChange}
                                handleChangeUsername={this.handleLoginUsernameChange}
                                handleLoginClick={this.handleLoginClick}
                                handleRegisterClick={this.handleLoginRegisterClick} />
                            : <RegisterComponent
                                registerError={this.state.registerError}
                                handleRegisterBack={this.handleRegisterBack}
                                handleChangePassword={this.handleRegisterPasswordChange}
                                handleChangeUsername={this.handleRegisterUsernameChange}
                                handleRegisterClick={this.handleRegisterClick} />
                        }
                        <button onClick={()=>{
                            console.log(this.props.token)
                        }}>click to show current token from state</button>
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token
    }
}
function mapDispatchToProps(dispatch) {
    return {
        getData: () => dispatch(getData()),
        setToken: (token) => dispatch(setToken(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingContainer);

