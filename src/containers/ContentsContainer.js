import React from 'react'
import api from '../services/ApiService'
import { connect } from 'react-redux'
import ContentListComponent from '../components/ContentListComponent'
import CreateContentComponent from '../components/CreateContentComponent'
import {Button} from 'reactstrap'
import { setToken } from "../actions/index";

class ContentsContainer extends React.Component {
    constructor(props){
        super(props)
        this.API = this.props.api
        this.state = {
            contentList: [],
            createContentValue:{
                name:"",
                status:"",
                category:"",
                author:"",
                content:""
            }
        }
    }

    /*
        CREATE CONTENT INPUT
    */
    handleCreateNameChange = (event) => {
        this.setState({
            createContentValue : Object.assign(this.state.createContentValue, {name:event.target.value})
        })
    }
    handleCreateStatusChange = (event) => {
        this.setState({
            createContentValue : Object.assign(this.state.createContentValue, {status:event.target.value})
        })
    }
    handleCreateCategoryChange = (event) => {
        this.setState({
            createContentValue : Object.assign(this.state.createContentValue, {category:event.target.value})
        })
    }
    handleCreateAuthorChange = (event) => {
        this.setState({
            createContentValue : Object.assign(this.state.createContentValue, {author:event.target.value})
        })
    }
    handleCreateContentChange = (event) => {
        this.setState({
            createContentValue : Object.assign(this.state.createContentValue, {content:event.target.value})
        })
    }
    handleCreateContentClick = (event) => {
        this.fetchCreateData(this.state.createContentValue)
    }

    /* DELETE CONTENT */
    handleDeleteContentClick = (contentID) => {
        this.fetchDeleteData(contentID)
    }

    /* LOGOUT */
    handleLogoutClick = (event) => {
        this.props.setToken(null)
        this.props.history.push('/login')
    }

    /* FETCH API */
    fetchCreateData = (createInfo) => {
        this.API.post(`/content/create`,createInfo)
        .then(res => {
            console.log(res)
            this.fetchAllData()
        },
            err => {
                console.log(err)
            })
    }
    fetchAllData = () => {
        this.API.post(`/content/getall`)
        .then(res => {
            console.log(res)
            this.setState({
                contentList: res.data
            })
        },
            err => {
                console.log(err)
            })
    }
    fetchDeleteData = (contentID) => {
        this.API.post(`/content/delete`,{contentID:contentID})
        .then(res => {
            console.log(res)
            this.fetchAllData()
        },
            err => {
                console.log(err)
            })
    }

    componentDidMount() {
        this.fetchAllData()
    }
    render() {
        return (
            <div>
                <CreateContentComponent 
                    handleNameChange={this.handleCreateNameChange.bind(this)}
                    handleStatusChange={this.handleCreateStatusChange.bind(this)}
                    handleCategoryChange={this.handleCreateCategoryChange.bind(this)}
                    handleAuthorChange={this.handleCreateAuthorChange.bind(this)}
                    handleContentChange={this.handleCreateContentChange.bind(this)}
                    handleCreateContentClick={this.handleCreateContentClick.bind(this)} />
                <div>
                    <ContentListComponent 
                    contentList={this.state.contentList} 
                    handleDeleteContentClick={this.handleDeleteContentClick.bind(this)}/>
                </div>
                <Button onClick={this.handleLogoutClick.bind(this)} style={{backgroundColor:"#B75D69"}}>LOGOUT</Button>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        token: state.token
    }
}
function mapDispatchToProps(dispatch) {
    return {
        setToken: token => dispatch(setToken(token))
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(ContentsContainer);