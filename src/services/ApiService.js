import { TEST_SERVER_PATH } from '../constants/api-config'
import axios from 'axios'
import configureStore from '../store/index'
const { store, persistor } = configureStore()

const api = (token) => {
  return axios.create({
    baseURL: TEST_SERVER_PATH,
    headers: {
      'Content-Type': 'application/json'
      ,'x-access-token': token
    }
  })
}
export default api

