const { resolve } = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    mode: "development",
    output: {
      filename: 'bundle.js',
      path: resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
              test: /\.js$/,
              exclude: /(node_modules)/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env', '@babel/preset-react'],
                },
              },
            },{
              test: /\.css$/,
               use: ['style-loader', 'css-loader']
            } 
          ],
    },
    plugins: [
        new HtmlWebpackPlugin({
          template: 'src/index.html',
        }),
      ],
      devServer: { historyApiFallback: true, contentBase: './' }
  }