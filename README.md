# Test Web
This web project create by using webpack

## dependencies

yarn add webpack webpack-cli

yarn add path

yarn add react react-dom

### webpack loader
to transform code with some syntax such as ES6, JSX, CSS, SASS to work

yarn add babel-core babel-loader babel-preset-env babel-preset-react --dev

### html-webpack-plugin 
webpack plugin to build index.html file everytime we build project
that we will get dist/index.html

### Webpack dev server
to create virtual server for developing 
this provides web-app auto refresh when we edit our code
yarn add webpack-dev-server --dev 

### REACT REDUX
yarn add react-redux
#### Provider
React Redux provides <Provider />, which makes the Redux store available to the rest of your app:
#### connect()
React Redux provides a connect function for you to connect your component to the store.

#### install redux and set up store
yarn add redux

#### reducer 
reducer produce the state of the application
which has to be pure function that return new value of the state ** important reducer not change state value but return the new one **
reducer is a function that take two parameters, state and action.

#### action 
reducer produce the state of the application but how does a reducer know when to generate the next state?
** only way to change the state of the application is by sending a signal to the store.
This signal is "action"
So dispatching an action means sending out a signal to the store.
Redux action is nothing more than JavaScript objects with two properties 'type' and 'payload'

type property is how state should change this is always required by redux
payload property describes what should change , this can be ommmited if you don't have new data to save to the store 

As a best practice we wrap every action within a function so that object creation is abstracted away
Such function takes the name of action creator

type property is a string. Strings are prone to typos and duplicates and for this reason it’s better to declare actions as constants. 

#### Redux store methods
- getState 
reading the current state of the application
- dispatch 
dispatching an action
- subscribe
listening on state changes

#### connect
Connect a React Component with redux store
we will use connect with two or three arguments depending on the case 
- mapStateToProps
connect a part of the Redux state to the props of a React component
- mapDispatchToProps
connect Redux action to the React props

#### redux thunk
With a plain basic Redux store, you can only do simple synchronous updates by dispatching an action. 
Middleware extend the store's abilities, and let you write async logic that interacts with the store.
yarn add redux-thunk

#### redux persist
to keep store to local storage 

### react router 
yarn add react-router-dom

### bootstrap reactstrap
Bootstrap has dependencies and we need to also install webpack loaders for styles, sass
yarn add jquery popper.js

Then we install webpack loaders

yarn add style-loader css-loader postcss-loader precss autoprefixer sass-loader

### chrome debugger 
https://code.visualstudio.com/docs/nodejs/reactjs-tutorial

## references
(Medium blog from THiNKNET Engineering)[https://engineering.thinknet.co.th/tutorial-%E0%B9%80%E0%B8%8B%E0%B9%87%E0%B8%95%E0%B8%AD%E0%B8%B1%E0%B8%9E-webpack-%E0%B9%81%E0%B8%A5%E0%B8%B0-react-%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87%E0%B9%81%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%A3%E0%B8%B4%E0%B9%88%E0%B8%A1%E0%B8%95%E0%B9%89%E0%B8%99%E0%B8%88%E0%B8%99-deploy-fa3d53e96469]

## problem found during developing

### Emitted 'error' event on FSWatcher instance at: ...

This problem is from ENOSPEC 

to avoid using ENOSPEC

> echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

### Support for the experimental syntax 'classProperties' isn't currently enabled (5:8):

yarn add @babel/plugin-proposal-class-properties --dev

and in .babelrc

"plugins": [
"@babel/plugin-proposal-class-properties"
]```

### regeneratorRuntime is not defined when use async with babel 
at this point you can not use async and await it will throw err "regeneratorRuntime is not defined"
from babel github in this issue 
theere is an answer to use 

yarn add @babel/plugin-transform-runtime --dev;
yarn add @babel/runtime;

for the dependencies and having the following in .babelrc

"plugins": [
    ["@babel/plugin-transform-runtime",
      {
        "regenerator": true
      }
    ]
  ],

### Webpack dev server React Content Security Policy error

add devServer: { historyApiFallback: true, contentBase: './' } to webpack.config.js





